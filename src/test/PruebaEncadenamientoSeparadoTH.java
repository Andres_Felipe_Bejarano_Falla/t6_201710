package test;

import junit.framework.TestCase;
import model.data_structures.EncadenamientoSeparadoTH;
import model.logic.GeneradorDeDatos;

public class PruebaEncadenamientoSeparadoTH extends TestCase {

	private EncadenamientoSeparadoTH<Integer, Integer> h;
	private GeneradorDeDatos gd = new GeneradorDeDatos();
	private int n = 0;
	private int k = 100;
	
	public void setupEscenario1() {
		h = new EncadenamientoSeparadoTH<>(0);
	}

	public void setupEscenario2() {
		h = new EncadenamientoSeparadoTH<>(5);
		h.insertar(12, 3);
		h.insertar(13, 4);
		h.insertar(11, 2);
		h.insertar(10, 1);
	}

	public void setupEscenario3() {
		h = new EncadenamientoSeparadoTH<>(k);
	}

	public void testAgregar1() {
		setupEscenario1();
		try {
			h.insertar(100, 5);
			fail();
		} catch (Exception e) {
		}
	}

	public void testAgregar2() {
		setupEscenario2();
		assertEquals(1, h.darValor(10).intValue());
		assertEquals(2, h.darValor(11).intValue());
		assertEquals(3, h.darValor(12).intValue());
		assertEquals(4, h.darValor(13).intValue());
	}

	public void testAgregar3() {
		setupEscenario3();
		Integer[] i = gd.generarNumeros(n);
		int c = 0;
		for (Integer n : i) {
			h.insertar(c++, n);
		}
		c = 0;
		for (Integer n : i) {
			assertEquals(n.intValue(), h.darValor(c++).intValue());
		}
	}

	public void testAgregar4() {
		// test eliminacion
		setupEscenario2();
		assertEquals(4, h.darTamanio());
		
		h.insertar(11, null);
		assertEquals(3, h.darTamanio());
		assertNull(h.darValor(11));
		
		h.insertar(10, null);
		assertNull(h.darValor(10));
		assertEquals(2, h.darTamanio());

	}
	 public void testAgregar5() {
			setupEscenario3();
			Integer[] i = gd.generarNumeros(n);
			int c = 0;
			for (Integer n : i) {
				h.insertar(c++, n);
			}
			c = 0;
			for (Integer n : i) {
				h.insertar(c++, null);
			}
			assertEquals(0, h.darTamanio());
	 }

	 public void testTieneLlave1() {
	 // 1
	 setupEscenario1();
	 assertFalse(h.tieneLlave(11));
	 // 2
	 setupEscenario2();
	 assertTrue(h.tieneLlave(10));
	 assertFalse(h.tieneLlave(21));
	 // 3
	 setupEscenario3();
	 assertFalse(h.tieneLlave(10));
	 assertFalse(h.tieneLlave(20));
	 }
	
	 public void testTieneLlave2() {
	 // 1
	 setupEscenario1();
	 assertFalse(h.tieneLlave(100));
	 assertFalse(h.tieneLlave(11));
	 // 2
	 setupEscenario2();
	 h.insertar(50, 2);
	 assertTrue(h.tieneLlave(50));
	 assertFalse(h.tieneLlave(21));
	 // 3
	 setupEscenario3();
	 h.insertar(20, 5);
	 h.insertar(21, 5);
	 assertTrue(h.tieneLlave(20));
	 assertTrue(h.tieneLlave(21));
	 }
	
	 public void testDarTamanio() {
	 // 1
	 setupEscenario1();
	 assertEquals(0, h.darTamanio());
	 // 2
	 setupEscenario2();
	 assertEquals(4, h.darTamanio());
	 // 3
	 setupEscenario3();
	 assertEquals(n, h.darTamanio());
	 }
	
	 public void testEstaVacia() {
	 // 1
	 setupEscenario1();
	 assertTrue(h.estaVacia());
	 // 2
	 setupEscenario2();
	 assertFalse(h.estaVacia());
	 // 3
	 setupEscenario3();
	 assertTrue(h.estaVacia());
	 }
	
	
}

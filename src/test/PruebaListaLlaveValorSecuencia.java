package test;

import java.util.Random;
import junit.framework.TestCase;
import model.data_structures.ListaLlaveValorSecuencia;

public class PruebaListaLlaveValorSecuencia extends TestCase {

	private ListaLlaveValorSecuencia<Integer, String> lista;
	Random j = new Random();

	public void setupEscenario1() {
		lista = new ListaLlaveValorSecuencia<>();
	}

	public void setupEscenario2() {
		lista = new ListaLlaveValorSecuencia<>();
		lista.insertar(10, "a");
	}

	public void setupEscenario3() {
		lista = new ListaLlaveValorSecuencia<>();
		for (int i = 0; i < 20; i++)
			lista.insertar(i, j.nextInt(1000000) + "");
	}

	public void testAgregar1() {
		setupEscenario1();
		lista.insertar(10, "hola");
		assertEquals("hola", lista.darValor(10));
	}

	public void testAgregar2() {
		setupEscenario2();
		lista.insertar(5, "hola");
		assertEquals("hola", lista.darValor(5));
		assertEquals("a", lista.darValor(10));
	}

	public void testAgregar3() {
		// test reemplazo
		setupEscenario2();
		lista.insertar(10, "hola");
		assertEquals("hola", lista.darValor(10));
	}

	public void testAgregar4() {
		// test eliminacion
		setupEscenario2();
		lista.insertar(10, null);
		assertEquals(0, lista.darTamanio());
		assertNull(lista.darValor(10));
	}

	public void testAgregar5() {
		// test eliminacion
		setupEscenario1();
		lista.insertar(10, null);
		assertEquals(0, lista.darTamanio());
		assertNull(lista.darValor(10));
	}

	public void testAgregar6() {
		// test eliminacion
		setupEscenario3();
		lista.insertar(10, null);
		assertEquals(19, lista.darTamanio());
		assertNull(lista.darValor(10));
	}

	public void testTieneLlave1() {
		// 1
		setupEscenario1();
		assertFalse(lista.tieneLlave(11));
		// 2
		setupEscenario2();
		assertTrue(lista.tieneLlave(10));
		assertFalse(lista.tieneLlave(11));
		// 3
		setupEscenario3();
		assertTrue(lista.tieneLlave(10));
		assertFalse(lista.tieneLlave(20));
	}

	public void testTieneLlave2() {
		// 1
		setupEscenario1();
		lista.insertar(2, "a");
		assertTrue(lista.tieneLlave(2));
		assertFalse(lista.tieneLlave(11));
		// 2
		setupEscenario2();
		lista.insertar(2, "a");
		assertTrue(lista.tieneLlave(2));
		assertFalse(lista.tieneLlave(11));
		// 3
		setupEscenario3();
		lista.insertar(20, "a");
		assertTrue(lista.tieneLlave(20));
		assertFalse(lista.tieneLlave(21));
	}

	public void testDarTamanio() {
		// 1
		setupEscenario1();
		assertEquals(0, lista.darTamanio());
		// 2
		setupEscenario2();
		assertEquals(1, lista.darTamanio());
		// 3
		setupEscenario3();
		assertEquals(20, lista.darTamanio());
	}

	public void testEstaVacia() {
		// 1
		setupEscenario1();
		assertTrue(lista.estaVacia());
		// 2
		setupEscenario2();
		assertFalse(lista.estaVacia());
		// 3
		setupEscenario3();
		assertFalse(lista.estaVacia());
	}
}

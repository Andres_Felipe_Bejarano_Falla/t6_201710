package model.VO;

public class VOInfoPelicula {

	private String titulo;
	private int anio;
	private String director;
	private String actores;
	private double ratingIMDB;

	public VOInfoPelicula(String t, int a, String d, String ac, double r) {
		titulo = t;
		anio = a;
		director = d;
		actores = ac;
		ratingIMDB = r;
	}
 
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public int getAnio() {
		return anio;
	}

	public void setAnio(int anio) {
		this.anio = anio;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getActores() {
		return actores;
	}

	public void setActores(String actores) {
		this.actores = actores;
	}

	public double getRatingIMDB() {
		return ratingIMDB;
	}

	public void setRatingIMDB(double ratingIMDB) {
		this.ratingIMDB = ratingIMDB;
	}

	public String toString() {
		return "[ " + titulo + " | " + anio + " | " + director + " | " + actores + " | " + ratingIMDB + " ]";
	}

}

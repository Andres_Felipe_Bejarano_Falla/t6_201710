package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	NodoSencillo<T> first;
	NodoSencillo<T> current;
	int size;

	public ListaEncadenada() {
		this.first = this.current = null;
		size = 0;
	}

	public ListaEncadenada(T[] s) {
		this.first = this.current = null;
		size = 0;
		for (T si : s) {
			agregarElementoFinal(si);
		}
	}

	public Iterator<T> iterator() {
		return new myIterator<>(first);
	}

	public void agregarElementoFinal(T elem) {
		if (first == null) {
			first = current = new NodoSencillo<T>(null, elem);
			size = 1;
			return;
		}

		while (current.next != null) {
			current = current.next;
		}
		current.setNext(new NodoSencillo<T>(null, elem));
		current = current.next;
		size++;
	}

	public void agregarALInicio(T elem) {
		if (first == null) {
			first = current = new NodoSencillo<T>(null, elem);
			size = 1;
			return;
		} else {
			first = new NodoSencillo<T>(first, elem);
		}
		current = first;
		size++;
	}

	public void removeInicial() {
		if (first != null) {
			first = first.getNext();
			size--;
		}
	}

	public T darElemento(int pos) {
		if (pos == 0 && first != null)
			return first.getItem();
		if (pos < size && pos > 0) {
			current = first.getNext();
			pos--;
			while (pos > 0) {
				current = current.getNext();
				pos--;
			}
			return current.getItem();
		}
		return null;
	}

	public int darNumeroElementos() {
		return size;
	}

	public T darElementoPosicionActual() {
		return current.getItem();
	}

	public boolean avanzarSiguientePosicion() {
		if (current.next == null)
			return false;
		current = current.next;
		return true;
	}

	public boolean retrocederPosicionAnterior() {
		NodoSencillo<T> temp;
		if (current.equals(first))
			return false;
		temp = first.getNext();
		while (!temp.getNext().equals(current)) {
			temp = temp.getNext();
		}
		current = temp;
		return true;
	}

	private class myIterator<T> implements Iterator<T> {

		private NodoSencillo<T> current;

		public myIterator(NodoSencillo<T> first) {
			current = first;
		}

		public boolean hasNext() {

			return current != null;
		}

		public T next() {
			if (hasNext()) {
				T item = current.item;
				current = current.next;
				return item;
			}
			return null;
		}
	}

	static class NodoSencillo<T> {
		NodoSencillo<T> next;
		T item;

		public NodoSencillo(NodoSencillo<T> next, T e) {
			this.next = next;
			this.item = e;
		}

		public NodoSencillo<T> getNext() {
			return next;
		}

		public void setNext(NodoSencillo<T> next) {
			this.next = next;
		}

		public T getItem() {
			return item;
		}

		public void setItem(T item) {
			this.item = item;
		}
	}

	public void set(T e, int i) {
		if (i == 0 && first != null)
			first.setItem(e);
		if (i < size && i > 0) {
			current = first.getNext();
			i--;
			while (i > 0) {
				current = current.getNext();
				i--;
			}
			current.setItem(e);
		}
	}

	public void deleteElemento(T elm) {
		if (size == 0)
			return;
		if (first.item.equals(elm)) {
			first = first.next;
			size--;
			return;
		}
		current = first;
		while (current.next != null) {
			if (current.next.item.equals(elm)) {
				current.next = current.next.next;
				size--;
				return;
			}
			current = current.next;
		}
	}

	public boolean tieneElemento(T elm) {
		if (size == 0)
			return false;
		if (first.item.equals(elm)) {
			return true;
		}
		current = first;
		while (current.next != null) {
			if (current.next.item.equals(elm)) {
				return true;
			}
			current = current.next;
		}
		return false;
	}

	public T eliminarElemento(int pos) {
		T res = null;
		if (pos == 0 && first != null) {
			res = first.getItem();
			size--;
			first = first.getNext();
		}
		if (pos < size && pos > 0) {
			current = first.getNext();
			pos--;
			while (pos > 0) {
				current = current.getNext();
				pos--;
			}
			res = current.getItem();
			size--;
		}
		return res;
	}

	public void setItem(T e, int pos) {
	}
}

package model.data_structures;

import java.util.Iterator;

public class ListaLlaveValorSecuencia<K, V> {

	private ListaEncadenada<K> llaves;
	private NodoSencillo<K, V> primero;
	private NodoSencillo<K, V> actual;

	public ListaLlaveValorSecuencia() {
		llaves = new ListaEncadenada<>();
		primero = null;
	}

	public int darTamanio() {
		return llaves.size;
	}

	public boolean estaVacia() {
		return llaves.size == 0;
	}

	public boolean tieneLlave(K llave) {
		// return llaves.tieneElemento(llave);
		for (K k : llaves) {
			if (k.equals(llave))
				return true;
		}
		return false;
	}

	public V darValor(K llave) {
		if (estaVacia())
			return null;
		if (primero.key.equals(llave))
			return primero.value;
		actual = primero.next;
		while (actual != null) {
			if (actual.key.equals(llave))
				return actual.value;
			actual = actual.next;
		}
		return null;
	}

	public void insertar(K pKey, V pValue) {
		if (pValue == null) {
			if (estaVacia())
				return;
			if (primero.key.equals(pKey)) {
				primero = primero.next;
				llaves.removeInicial();
				return;
			}
			actual = primero;
			while (actual.next != null) {
				if (actual.next.key.equals(pKey)) {
					actual.next = actual.next.next;
					llaves.deleteElemento(pKey);
					return;
				}
				actual = actual.next;
			}
		} else {
			if (estaVacia()) {
				primero = new NodoSencillo<K, V>(pValue, pKey);
				llaves.agregarElementoFinal(pKey);
				return;
			}
			else if (primero.key.equals(pKey)) {
				primero.setItem(pValue);
				return;
			}
			actual = primero;
			while (actual.next != null) {
				if (actual.next.getKey().equals(pKey)) {
					actual.next.setItem(pValue);
					return;
				}
				actual = actual.next;
			}
			actual.setNext(new NodoSencillo<K, V>(pValue, pKey));
			llaves.agregarElementoFinal(pKey);
			return;
		}
	}

	public Iterable<K> llaves() {
		return new Iterable<K>() {
			public Iterator<K> iterator() {
				return llaves.iterator();
			}
		};
	}
	
	public Iterable<NodoSencillo<K, V>> nodos() {
		return new Iterable<NodoSencillo<K,V>>() {
			public Iterator<NodoSencillo<K, V>> iterator() {
				return new Iterator<NodoSencillo<K,V>>() {
					NodoSencillo<K, V> current = primero;

					public boolean hasNext() {
						return current!=null;
					}

					public NodoSencillo<K, V> next() {
						NodoSencillo<K, V> n = current;
						current = current.getNext();
						return n;
					}
				};
			}
		};
	}

	static class NodoSencillo<K, V> {
		private NodoSencillo<K, V> next;
		private V value;
		private K key;

		public NodoSencillo(V e, K k) {
			this.next = null;
			this.key = k;
			this.value = e;
		}

		public NodoSencillo<K, V> getNext() {
			return next;
		}

		public void setNext(NodoSencillo<K, V> next) {
			this.next = next;
		}

		public V getItem() {
			return value;
		}

		public K getKey() {
			return key;
		}

		public void setItem(V item) {
			this.value = item;
		}
	}
}
package model.data_structures;

import java.util.Iterator;

import model.data_structures.ListaLlaveValorSecuencia.NodoSencillo;

public class EncadenamientoSeparadoTH<K, V> {

	Object[] slots;
	int M;

	public EncadenamientoSeparadoTH(int m) {
		slots = new Object[m];
		for (int i = 0; i < m; i++)
			slots[i] = new ListaLlaveValorSecuencia<>();
		M = m;
	}

	public int darTamanio() {
		int temp = 0;
		for (int i = 0; i < slots.length; i++) {
			temp += ((ListaLlaveValorSecuencia<K, V>) slots[i]).darTamanio();
		}
		return temp;
	}

	public boolean estaVacia() {
		return darTamanio() == 0;
	}

	public boolean tieneLlave(K llave) {
		if(M ==0)return false;
		return ((ListaLlaveValorSecuencia<K, V>) slots[hash(llave)]).tieneLlave(llave);
	}

	public V darValor(K llave) {
		if (!tieneLlave(llave))
			return null;
		else
			return ((ListaLlaveValorSecuencia<K, V>) slots[hash(llave)]).darValor(llave);
	}

	public void insertar(K llave, V valor) {
		if (fact() > 500)
			reHash();
		((ListaLlaveValorSecuencia<K, V>) slots[hash(llave)]).insertar(llave, valor);
	}

	public Iterable<K> llaves() {
		return new Iterable<K>() {
			public Iterator<K> iterator() {
				return new myIterator(slots);
			}
		};
	}

	private class myIterator implements Iterator<K> {
		ListaLlaveValorSecuencia<K, V>[] slots;
		int current;
		Iterator<K> ite = null;

		public myIterator(Object[] c) {
			slots = (ListaLlaveValorSecuencia<K, V>[]) c;
			current = 0;
			if (slots[current] != null)
				ite = slots[current].llaves().iterator();
		}

		public boolean hasNext() {
			boolean b = false;
			if (ite != null) {
				b = ite.hasNext();
				while (!b && current + 1 < slots.length) {
					ite = slots[++current].llaves().iterator();
					b = ite.hasNext();
				}
			}
			return b;
		}

		public K next() {
			if (hasNext())
				return ite.next();
			return null;
		}
	}

	public int hash(K llave) {
		if (llave instanceof String) {
			return ((String) llave).length()% M;
		} else if (llave instanceof Integer) {
			return ((Integer) llave).intValue() % M;
		} else {
			return llave.hashCode() % M;
		}
	}

	public int[] darLongitudListas() {
		int[] r = new int[slots.length];
		for (int i = 0; i < slots.length; i++)
			r[i] = ((ListaLlaveValorSecuencia<K, V>) slots[i]).darTamanio();
		return r;
	}

	private void reHash() {
		M *= 2;
		Object[] newSlots = new Object[M];
		for (int i = 0; i < M; i++)
			newSlots[i] = new ListaLlaveValorSecuencia<K,V>();
		
		Iterator<NodoSencillo<K, V>> ite = null;
		
		for (int i = 0; i < slots.length; i++) {
			ListaLlaveValorSecuencia<K, V> l = (ListaLlaveValorSecuencia<K, V>) slots[i];
			ite = (Iterator<NodoSencillo<K, V>>) l.nodos().iterator();
		
			while (ite.hasNext()) {
				NodoSencillo<K, V> n = ite.next();
				((ListaLlaveValorSecuencia<K, V>) newSlots[hash(n.getKey())]).insertar(n.getKey(), n.getItem());
			}
			
		}
		slots = newSlots;
	}

	private double fact() {
		return darTamanio() / M;
	}
}

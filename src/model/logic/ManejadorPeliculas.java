package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.PrintStream;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import model.VO.VOInfoPelicula;
import model.data_structures.EncadenamientoSeparadoTH;

public class ManejadorPeliculas {

	public static final String RUTA = "./data/links_json.json";

	EncadenamientoSeparadoTH<Integer, VOInfoPelicula> hola = new EncadenamientoSeparadoTH<Integer, VOInfoPelicula>(5);

	public void cargarDatos(String ruta) {
		try {
			FileReader fr = new FileReader(ruta);
			BufferedReader buff = new BufferedReader(fr);
			JsonArray ja = (JsonArray) (new JsonParser()).parse(buff);

			for (JsonElement jsonElement : ja) {

				JsonObject jo = (JsonObject) jsonElement;
				JsonElement id = jo.get("movieId");
				int movieId = id.getAsInt();

				JsonObject info = (JsonObject) jo.get("imdbData");

				String anio = info.get("Year").getAsString().substring(0, 4);
				int up = Integer.parseInt(anio);

				String rating = info.get("imdbRating").getAsString();
				double j = 0;
				if (!rating.equals("N/A"))
					j = Double.parseDouble(rating);

				VOInfoPelicula h = new VOInfoPelicula(info.get("Title").getAsString(), up,
						info.get("Director").getAsString(), info.get("Actors").getAsString(), j);

				hola.insertar(movieId, h);

				buff.close();
				fr.close();
			}
		} catch (Exception e) {
			System.err.println(e);
		}
	}
	
	public EncadenamientoSeparadoTH<Integer, VOInfoPelicula> darDatos(){
		return hola;
	}
	
	public VOInfoPelicula busqueda(Integer llave){
		return hola.darValor(llave);
	}

//	public static void main(String[] args) {
//		ManejadorPeliculas k = new ManejadorPeliculas();
//		long time=System.currentTimeMillis();
//		k.cargarDatos(RUTA);
//		System.out.println(System.currentTimeMillis()-time);
//		time=System.currentTimeMillis();
//		System.out.println(k.busqueda(1));
//		System.out.println(k.busqueda(164979));
//		System.out.println(k.busqueda(148881));
//		System.out.println(k.busqueda(132952));
//		System.out.println(k.busqueda(5832));
//		double d=(System.currentTimeMillis()-time);
//		System.out.println(d);
//	}
}

package model.logic;

import java.util.*;

import model.data_structures.EncadenamientoSeparadoTH;
import model.data_structures.ListaEncadenada;

public class Experimento {

	static int K = 100;
	static int N = 10000;
	static int f = 10;

	public static GeneradorDeDatos gd = new GeneradorDeDatos();
	public static String[] k;
	public static Integer[] v;
	public static EncadenamientoSeparadoTH<String, Integer> th = new EncadenamientoSeparadoTH<>(f);
	public static Histogram h = new Histogram(f);

	public static void main(String[] args) {

		for (int i = 1000; i < 20001; i+=1000) {
			th = new EncadenamientoSeparadoTH<>(f);
			double d=0;
			for(int j=0;j<20;j++){
				k=gd.generarCadenas(i, K);
				v=gd.generarNumeros(i);
				long time = System.currentTimeMillis();
				for (int ni = 0; ni < v.length; ni++) {
					th.insertar(k[ni], v[ni]);
				}
				d+=System.currentTimeMillis()-time;
			}
			d/=20;
			System.out.println(i+"        "+d);
		}

		//		System.out.println("----------------------------Pruebas----------------------------");
		//		
		//		k = gd.generarCadenas(N, K);
		//		System.out.println("-----Genero LLaves-----");
		//		v = gd.generarNumeros(N);
		//		System.out.println("-----Genero Valores-----");
		//		long time = System.currentTimeMillis();
		//		for (int i = 0; i < v.length; i++) {
		//			th.insertar(k[i], v[i]);
		//		}
		//		System.out.println("Tiempo de Insercion de los datos:" + (System.currentTimeMillis() - time));
		//		System.out.print("Las longitudes son: ");
		//
		//		int[] longs = th.darLongitudListas();
		//		for (int j = 0; j < longs.length; j++) {
		//			h.addDataPoint(j, longs[j]);
		//			System.out.print(longs[j] + "| ");
		//		}
		//		h.draw();
		//		System.out.println(" ");
		//		System.out.println(" ");

		//System.out.println(" ███۞███████ ]▄▄▄▄▄▄▄▄▄▄▄▄▃ \n ▂▄▅█████████▅▄▃▂ \n I███████████████████]. \n ◥⊙▲⊙▲⊙▲⊙▲⊙▲⊙▲⊙◤... ");
		//		System.out.println(" ");
		//		System.out.println(" ");
		//		System.out.println(" ");
		//		System.out.println(" ");
		//		System.out.println(" ");
		//		System.out.println(" ");
		//		System.out.println("     ╔╦╗─╔╦╗─╔═╗╔══╗──╔╗╔╗╔╗──                          ___");
		//		System.out.println("     ║╩╠═╣╠╣ ║╔╬╬╗╔╩╗ ║║║║║║                            .~))>>");
		//		System.out.println("     ║╦║╬║║║ ║║║║║║╬║ ║║║║║║                          .~)>>");
		//		System.out.println("     ╚╩╩═╩╩╝ ╚╝╚═╩╩═╝ ╚╝╚╝╚╝                          .~))))>>>");
		//		System.out.println("     ─────────────────╔╗╔╗╔╗──                      .~))>>             ___");
		//		System.out.println("                      ╚╝╚╝╚╝                    .~))>>)))>>      .-~))>>");
		//		System.out.println("                                              .~)))))>>       .-~))>>)>");
		//		System.out.println("                                            .~)))>>))))>>  .-~)>>)>");
		//		System.out.println("                        )                 .~))>>))))>>  .-~)))))>>)>");
		//		System.out.println("                     ( )@@*)             //)>))))))  .-~))))>>)>");
		//		System.out.println("                   ).@(@@               //))>>))) .-~))>>)))))>>)>");
		//		System.out.println("                 (( @.@).              //))))) .-~)>>)))))>>)>");
		//		System.out.println("               ))  )@@*.@@ )          //)>))) //))))))>>))))>>)>");
		//		System.out.println("            ((  ((@@@.@@             |/))))) //)))))>>)))>>)>");
		//		System.out.println("           )) @@*. )@@ )   (\\_(\\ -\b  |))>)) //)))>>)))))))>>)>");
		//		System.out.println("         (( @@@(.@(@ .    _/`-`  ~|b |>))) //)>>)))))))>>)>");
		//		System.out.println("          )* @@@ )@*     (@) (@)  /\b|))) //))))))>>))))>>");
		//		System.out.println("        (( @. )@( @ .   _/       /  \b)) //))>>)))))>>>_._");
		//		System.out.println("         )@@ (@@*)@@.  (6,   6) / ^  \b)//))))))>>)))>>   ~~-.");
		//		System.out.println("      ( @jgs@@. @@@.*@_ ~^~^~, /\\  ^  \b/)>>))))>>      _.     `,");
		//		System.out.println("       ((@@ @@@*.(@@ .   \\^^^/' (  ^   \b)))>>        .'         `,");
		//		System.out.println("        ((@@).*@@ )@ )    `-'   ((   ^  ~)_          /             `,");
		//		System.out.println("          (@@. (@@ ).           (((   ^    `\\       |               `.");
		//		System.out.println("            (*.@*              / ((((        \\       \\      .         `.");
		//		System.out.println("                              /   (((((  \\    \\   _.-~\\    Y,         ;");
		//		System.out.println("                             /   / (((((( \\    \\.-~   _.`´´ _.-~`,       ;");
		//		System.out.println("                            /   /   `(((((()    )    (((((~      `,     ;");
		//		System.out.println("                          _/  _/      `---/   /'                  ;     ;");
		//		System.out.println("                      _.-~_.-~           /  /'                _.-~   _.'");
		//		System.out.println("                    ((((~~              / /'              _.-~ __.--~");
		//		System.out.println("                                       ((((          __.-~ _.-~");
		//		System.out.println("                                                   .'   .~~");
		//		System.out.println("                                                   :    ,'");
		//		System.out.println("                                                   ~~~~~");
		//		
		//		System.out.println(" ");
		//		System.out.println(" ");
		//		System.out.println(" ");
		//		System.out.println(" ");
		//		System.out.println(" ");
		//		System.out.println(" ");
		//		
		//		System.out.println("╔╦╗─╔╦╗─╔═╗╔══╗──╔╗╔╗╔╗──");
		//		System.out.println("║╩╠═╣╠╣ ║╔╬╬╗╔╩╗ ║║║║║║  ");
		//		System.out.println("║╦║╬║║║ ║║║║║║╬║ ║║║║║║  ");
		//		System.out.println("╚╩╩═╩╩╝ ╚╝╚═╩╩═╝ ╚╝╚╝╚╝  ");
		//		System.out.println("─────────────────╔╗╔╗╔╗──");
		//		System.out.println("                 ╚╝╚╝╚╝  ");
		//

	}
}

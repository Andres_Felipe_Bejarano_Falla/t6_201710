package model.logic;

import java.util.Random;

public class GeneradorDeDatos {

	public String[] generarCadenas(int N, int K) {
		String[] cadenas = new String[N];
		int x = 0;
		String m = "";
		Random rdn = new Random();
		for (int i = 0; i < N; i++) {
			x = rdn.nextInt(K-1);
			m += "a";
			for (int j = 0; j < x; j++)
				m += "a";
			cadenas[i] = m;
			m = "";
		}
		return cadenas;
	}
	
	public Integer[] generarNumeros(int N) {
		Random rdn = new Random();
		Integer[] numeros = new Integer[N];
		for (int i = 0; i < N; i++) {
			numeros[i] = rdn.nextInt(2000);
		}
		return numeros;
	}
}
